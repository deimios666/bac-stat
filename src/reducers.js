//import { combineReducers } from "redux";

import  actionTypes from "./actionTypes";


const initialState = {
  file: {
    loaded: false
  },
  drawer: {
    open: false
  }
}


const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TOGGLE_DRAWER:{
        const open = action.payload;
        return {...state, drawer:{...state.drawer, open:open} }
      }
    default:
      return state;
  }
}


const rootReducer = mainReducer;

export default rootReducer;
