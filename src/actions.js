import actionTypes from "./actionTypes";

const actions = {
  
  loadFileAction: content => ( {
    type: actionTypes.LOAD_FILE,
    payload: content
  }),
  
  exportStatAction: statName => ({
    type: actionTypes.EXPORT_STAT,
    payload: statName
  }),

  toggleDrawerAction: open => ({
    type: actionTypes.TOGGLE_DRAWER,
    payload: open
  })
}

export default actions;
