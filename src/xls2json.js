import XLSX from "xlsx";
import config from "./config";

const xls2json = (file,callback) => {
  const reader = new FileReader();
  reader.onload = e => {
    const data = new Uint8Array(e.target.result);
    const workbook = XLSX.read(data, {type: 'array'});
    const jsonData = XLSX.utils.sheet_to_json(workbook.Sheets[config.xls.sheetName], {header:config.xls.headers});
    callback(null,jsonData);
  }
  reader.readAsArrayBuffer(file);
}

export default xls2json;