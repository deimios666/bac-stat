import React from 'react';

import { connect } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import './App.css';

import actions from './actions';
import config from './config';
import xls2json from './xls2json';

const toggleDrawerAction = actions.toggleDrawerAction;

//create material-ui compatbile styles from normal style
const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

//define App as a React function
const App = (props) => {

  const classes = useStyles();

  //get the dispatch function from props (provided by redux)
  const dispatch = props.dispatch;

  //define the input reference for file upload
  let fileInput;

  const filterData = jsonData => {
    
    let data = jsonData;
    let found = false;
    let i = 0;
    //find the string "CNP" in the first cell of the first 6 rows
    for (i = 0;i < 6 && !found;i++){
      if (data[i].CNP === "CNP"){
        found = true;
      }
    }

    if (found){
      //"CNP" found, cut the lines above it
      data.splice(0,i);
      //Filter the fields we need
      const filteredData = data.map(
        item => {
          const retVal = {};
          config.xls.filteredHeader.forEach(header=>retVal[header]=item[header]);
          return retVal;
        }
      );

      return filteredData;
    }
    return null;
  }


  //an array reduce function that simulates countif
  const countIfInitial = (groupHeader,finalStat=false) => (acc,item) => {
    const groupHeaderValue = item[groupHeader];
    let accumulator = acc;

    let media;
    let status;

    if (finalStat) {
      media = item["MEDIA_FINALA"];
      status = item["STATUS_FINAL"];
    } else {
      media = item["MEDIA_INITIALA"];
      status = item["STATUS_INITIAL"];
    }

    if (!accumulator.hasOwnProperty(groupHeaderValue)){
      accumulator[groupHeaderValue] = {
        Inscris: 0,
        Prezent: 0,
        Neprezentat: 0,
        Eliminat: 0,
        Respins: 0,
        sub5: 0,
        i5: 0,
        Reusit: 0,
        i6: 0,
        i7: 0,
        i8: 0,
        i9: 0,
        i10: 0
      }
    }

      //  Inscris
      accumulator[groupHeaderValue].Inscris+=1;
      //  Prezent/Neprezentat
      if (media !== -2) {
        accumulator[groupHeaderValue].Prezent+=1;
      } else {
        accumulator[groupHeaderValue].Neprezentat+=1;
      }
      //  Eliminat
      if (media === -1) {
        accumulator[groupHeaderValue].Eliminat+=1;
      }
      
      //  Respins
      if (status === "Nepromovat") {
        accumulator[groupHeaderValue].Respins+=1;
        //  sub5
        if (media === -3) {
          accumulator[groupHeaderValue].sub5+=1;
        } else {
        //  i5
          accumulator[groupHeaderValue].i5+=1;
        }
      }
      //  Reusit
      if (status === "Promovat") {
        accumulator[groupHeaderValue].Reusit+=1;
      //  i6
      //  i7
      //  i8
      //  i9
      //  i10
        accumulator[groupHeaderValue]["i"+Math.trunc(media)]+=1;
      }
    return accumulator;
  }

  //code to handle the file upload
  const handleFileUpload = event => {
      if ( fileInput.files[0].name.endsWith('xlsx') ){
        xls2json(fileInput.files[0], (err, jsonData) => {
          if (err){
            console.err(err);
            return;
          }
          const filteredData = filterData(jsonData);
          const statInitial = filteredData.reduce(countIfInitial("SCOALA"),{});
          const statFinal = filteredData.reduce(countIfInitial("SCOALA",true),{});
          console.log("Stat initial pe unitati: ",statInitial);
          console.log("Stat final pe unitati: ",statFinal);
        })
      }
  }

  const toggleDrawer = (open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    //create a toggleDrawerAction and dispatch it to modify state
    dispatch(toggleDrawerAction(open));
  };

  //define the content of the side drawer
  const sideList = () => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(false)}
      onKeyDown={toggleDrawer(false)}
    >
      <List>
        <ListItem button>
          <ListItemText primary="Hello world" />
        </ListItem>
      </List>
    </div>
  );


  //return the main GUI
  return (
    <div className="App">
      <IconButton
            color="inherit"
            aria-label="Open drawer"
            edge="end"
            onClick={toggleDrawer(true)}
          >
        <MenuIcon />
      </IconButton>

      <Drawer open={props.drawer.open} onClose={toggleDrawer(false)}>
        {sideList()}
      </Drawer>

      <Container maxWidth="sm">
        <Box my={4}>
          <Typography variant="h4" component="h4" gutterBottom>
            Statistici Bacalaureat
          </Typography>
          <Typography variant="body1" component="p">
            Vă rugăm încărcați exportul excel din aplicația Bacalaureat
          </Typography>
          <Typography variant="body1" component="p">
            (Datele sunt prelucrate local în browser iar datele personale nu sunt importate)
          </Typography>
          <p>
            <input type="file" id="input" ref={el => ( fileInput = el )} onChange={handleFileUpload} />
          </p>
        </Box>
      </Container>
    </div>
  );
}


//we pass the whole global state to App
const mapState = state => state;


//export the App+state
export default connect(mapState)(App);
