const actionTypes = {
  LOAD_FILE:"LOAD_FILE",
  EXPORT_STAT:"EXPORT_STAT",
  TOGGLE_DRAWER: "TOGGLE_DRAWER",
}

export default actionTypes;
